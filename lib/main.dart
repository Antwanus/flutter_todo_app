import 'package:flutter/material.dart';
import 'package:flutter_todo_app/screens/tasks_screen.dart';
import 'package:flutter_todo_app/services/task_data_service.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TaskDataService>(
      create: (context) => TaskDataService(),
      child: const MaterialApp(
        home: TasksScreen(),
      ),
    );
  }
}

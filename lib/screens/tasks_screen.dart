import 'package:flutter/material.dart';
import 'package:flutter_todo_app/screens/add_task_bottom_sheet.dart';
import 'package:flutter_todo_app/services/task_data_service.dart';
import 'package:flutter_todo_app/widgets/tasks_list_view.dart';
import 'package:provider/provider.dart';

class TasksScreen extends StatelessWidget {
  const TasksScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.lightBlue,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.pinkAccent,
          onPressed: () {
            showModalBottomSheet(
              backgroundColor: Colors.transparent,
              isScrollControlled: true,
              context: context,
              builder: (context) => const AddTaskBottomSheet(),
            );
          },
          child: const Icon(Icons.add),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(left: 20.0, top: 40.0, right: 20.0, bottom: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                    title: const Text(
                      'TODO_APP',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 40.0,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    leading: FloatingActionButton(
                      onPressed: () {
                        //todo navigation here
                      },
                      backgroundColor: Colors.blueGrey.shade300,
                      elevation: 0,
                      child: const Icon(Icons.list, color: Colors.white),
                    ),
                    subtitle: Text(
                      '${Provider.of<TaskDataService>(context).taskCount} tasks left to do',
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                child: const TasksListView(),
              ),
            ),
          ],
        ));
  }
}

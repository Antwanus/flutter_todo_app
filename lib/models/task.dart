class Task {
  final String name;
  bool _isDone = false;

  Task({required this.name});

  bool get isDone => _isDone;
  void toggleIsDone() => _isDone = !_isDone;
}

import 'package:flutter/material.dart';

class TaskListTile extends StatelessWidget {
  final bool isTaskDone;
  final String taskTitle;
  final void Function(bool?) checkBoxCallback;

  const TaskListTile({
    required this.isTaskDone,
    required this.taskTitle,
    required this.checkBoxCallback,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        taskTitle,
        style: TextStyle(decoration: isTaskDone ? TextDecoration.lineThrough : null),
      ),
      trailing: Checkbox(
        activeColor: Colors.blueAccent,
        value: isTaskDone,
        onChanged: checkBoxCallback,
      ),
    );
  }
}

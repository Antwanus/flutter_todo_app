import 'package:flutter/material.dart';
import 'package:flutter_todo_app/services/task_data_service.dart';
import 'package:flutter_todo_app/widgets/task_list_tile.dart';
import 'package:provider/provider.dart';

class TasksListView extends StatelessWidget {
  const TasksListView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Consumer<TaskDataService>(
        builder: (BuildContext context, taskDataService, Widget? child) {
      return ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return TaskListTile(
            taskTitle: taskDataService.getTaskTitleByIndex(index),
            isTaskDone: taskDataService.getIsTaskDoneByIndex(index),
            checkBoxCallback: (checkboxState) {
              taskDataService.checkTaskByIndex(index);
            },
          );
        },
        itemCount: taskDataService.getAllTasks().length,
      );
    });
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter_todo_app/models/task.dart';

class TaskDataService extends ChangeNotifier {
  final _tasksList = <Task>[
    Task(name: 'Buy chocolate'),
    Task(name: 'Buy toilet paper'),
    Task(name: 'Buy BTC'),
  ];

  void removeCheckedTasks() {
    _tasksList.removeWhere((task) => task.isDone);
    notifyListeners();
  }

  List<Task> getAllTasks() {
    return List.unmodifiable(_tasksList);
  }

  int get taskCount {
    return _tasksList.length;
  }

  void addTask(Task t) {
    _tasksList.add(t);
    notifyListeners();
  }

  void checkTaskByIndex(int i) {
    _tasksList[i].toggleIsDone();
    notifyListeners();
  }

  void deleteTask(Task t) {
    if (findTask(t) != null) {
      _tasksList.remove(t);
      notifyListeners();
    }
  }

  Task? findTask(Task t) {
    for (Task element in _tasksList) {
      if (element == t) {
        return element;
      }
    }
    return null;
  }

  String getTaskTitleByIndex(int i) {
    return _tasksList[i].name;
  }

  bool getIsTaskDoneByIndex(int i) {
    return _tasksList[i].isDone;
  }
}
